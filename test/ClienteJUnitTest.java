/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import venda.entity.Cliente;
import venda.facade.ClienteFacade;

/**
 *
 * @author andreani
 */
public class ClienteJUnitTest {
    
    public ClienteJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    
      @Test
     public void testaSelecaoPorPrefixo() {
         
         ClienteFacade clienteFacade = new ClienteFacade();
         
         Cliente cliente  = new Cliente();     
         cliente.setName("Alex");
         
         
         clienteFacade.adicionar(cliente);
         cliente  = new Cliente();     
         cliente.setName("Alessandro");
         clienteFacade.adicionar(cliente);
         
         cliente  = new Cliente();     
         cliente.setName("Alexandre");
         clienteFacade.adicionar(cliente);
         
         List<Cliente> clientes=clienteFacade.buscaPorPrefixo("al");
     
         
         
     }
}
