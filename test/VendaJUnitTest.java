/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import venda.Venda;
import venda.entity.Produto;
import venda.facade.ProdutoFacade;
import venda.facade.VendaFacade;

/**
 *
 * @author andreani
 */
public class VendaJUnitTest {
    
    public VendaJUnitTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
   @Test
   public void adicionarProduto() {
       
       Produto produto = new Produto();
       produto.setNome("bom-bril");
       produto.setPreco(2.3f);
       
       
       ProdutoFacade produtoFacade= new ProdutoFacade();
       
       produtoFacade.adicionar(produto);

   
   }
}
