/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venda.facade;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import venda.entity.Cliente;

/**
 *
 * @author andreani
 */
public class ClienteFacade {
    
    private EntityManagerFactory emf;
    
    public ClienteFacade(){
        emf=Persistence.createEntityManagerFactory("vendasPU");
    }
    
    
    public List<Cliente> buscaPorPrefixo(String prefixName){
    
       EntityManager em=emf.createEntityManager();
       Query query = em.createQuery("SELECT c.name FROM Cliente c WHERE c.name LIKE CONCAT('%',:prefixName,'%')");
       query.setParameter(prefixName, "prefixName");
       return query.getResultList();      
    }
    
    public void adicionar(Cliente cliente){
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(cliente);
        em.getTransaction().commit();
    }
        public List<Cliente>findByAll(){
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("Select p from Produto p");
        return query.getResultList();
    }
}
