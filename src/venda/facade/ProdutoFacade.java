/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venda.facade;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import venda.entity.Produto;
import venda.entity.Venda;

/**
 *
 * @author andreani
 */
public class ProdutoFacade {

    private EntityManagerFactory emf;
    
    public ProdutoFacade(){
        emf=Persistence.createEntityManagerFactory("vendasPU");
    }
    
    
    public void adicionar(Produto produto){
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(produto);
        em.getTransaction().commit();
    }    
    
    
    public List<Produto>findByAll(){
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("Select p from Produto p");
        return query.getResultList();
    }
}
