/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package venda.facade;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import venda.entity.Venda;

/**
 *
 * @author andreani
 */
public class VendaFacade {
    
    private EntityManagerFactory emf;
    
    
    public void adicionar(Venda venda){
        EntityManager em=emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(venda);
        em.getTransaction().commit();
    }
    
}
